import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class RegisterService {

  constructor(private http: Http) { }

  submit(data) {
    return this.http.post(`http://localhost:3000/user`,data)
      .map((res:Response) => res.json());
  }

}
