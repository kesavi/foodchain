import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {BrowserModule} from "@angular/platform-browser";
import {CustomMaterialModule} from "../custom-material/custom-material.module";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout';
import {RegisterService} from './services/register.service';
import { HttpModule } from '@angular/http';
@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    HttpModule
  ],
  declarations: [LoginComponent,
    RegisterComponent],
  exports: [
    LoginComponent,
    RegisterComponent
  ],
  providers: [RegisterService],
})
export class UserModule { }
