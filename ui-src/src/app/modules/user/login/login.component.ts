import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user= {username:'',password:'',remember:false};
  constructor(private router:Router) { }

  ngOnInit() {
  }
  onSubmit(){
    console.log("user:",this.user);
  }
  register(event){
    event.stopPropagation();
    this.router.navigate(['/register']);
  }
}
