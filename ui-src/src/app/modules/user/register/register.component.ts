import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router";import {RegisterService} from '.././services/register.service';
import {User} from '.././models/User';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm:FormGroup;
  user:User= new User();
  constructor(private fb:FormBuilder,
              private registerService:RegisterService,
              private router:Router) {

    console.log(this.user);
    this.createForm();
  }

  ngOnInit() {
  }
  createForm(){
    this.registerForm = this.fb.group({
      username: ['',[Validators.required]],
       telNum: [0,	[Validators.required]],
      email:['', [Validators.required,Validators.email]],
      agree: false,
      password: ['',[Validators.required]],
      street:''
    });
  }

  onSubmit(){
    let formValue=this.registerForm.value;
    console.log(formValue);
    console.log(this.user);
    this.user.username=formValue.username;
    this.user.password=formValue.password;
    this.user.contact.mobile= formValue.telNum;
    this.user.address.street= formValue.street;
    this.user.address.state= formValue.state;
    this.user.address.country= formValue.country;
    this.user.address.zip= formValue.zip;
    this.registerService.submit(this.user)
      .subscribe(result =>
        this.router.navigateByUrl('/register'),
        errmess => console.log(errmess));
  }

}
