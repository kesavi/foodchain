export class  User{
   username: String;
   password: String;
   contact= {email:String,
    phone:String,
    mobile:String};
  address= {street:String,
    state:String,
    country:String,
    zip:String};
  agree: boolean;
};
