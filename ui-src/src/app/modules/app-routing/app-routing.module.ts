import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule,Routes,RouterLink} from '@angular/router';
import {ROUTES} from './routes';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    RouterModule.forRoot(ROUTES)
  ],
  exports:[RouterModule],
  declarations: []
})
export class AppRoutingModule { }
