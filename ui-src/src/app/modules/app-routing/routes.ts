import {Routes} from '@angular/Router';
import {LoginComponent} from '.././user/login/login.component';
import {RegisterComponent} from '.././user/register/register.component';
export const ROUTES:Routes= [{
  path:'',
  redirectTo: 'login',
  pathMatch: 'full'
},{
  path:'login',
  component:LoginComponent,
},{
  path:'register',
  component:RegisterComponent,
}];
