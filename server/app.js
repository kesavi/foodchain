const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const dbConfig = require('./config/dbconfig');
const user = require('./controllers/user');

const router = express.Router();
const app = express();
mongoose.connect(dbConfig.database);
app.use(router);
app.use(cors());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/user',user);

app.get('/', (req,res) => {
    res.send("Invalid page");
});
const port = 3000;
app.listen(port, () => {
    console.log(`Starting the server at port ${port}`);
});