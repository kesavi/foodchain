const mongoose = require('mongoose');
const UserSchema = mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    Contact: [{email:{type:String},
                phone:{type:String,maxlength:13,minlength:13},
                mobile:{type:Number}}],
    address:[{street:String,state:String,country:String,zip:String}]
});

const user =module.exports= mongoose.model('user',UserSchema);
module.exports.addUser = (newUser, callback) => {
    newUser.save(callback);
}
module.exports.deleteUser = (id, callback) => {
    let query = {_id: id};
    BucketList.remove(query, callback);
}