const express = require('express');
const router = express.Router();
const user = require('../models/user');

router.get('/',(req,res)=>{
    res.send("no user yet");
});
router.post('/', (req,res,next) => {
    console.log(JSON.stringify(req.body));
    let newUser = new user({
        username: req.body.username,
        password: req.body.password,
        contact: req.body.contact,
        address: req.body.address
    });
    user.addUser(newUser,(err, list) => {
        if(err) {
            res.json({success: false, message: `Failed to create a new list. Error: ${err}`});

        }
        else
            res.json({success:true, message: "Added successfully."});

    });
});
module.exports= router;
